import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Introduction from './content/Introduction';
import ExampleBasic from './content/example/01-Basic';

function Content() {
  return (
    <Switch>
      <Route exact path="/" component={Introduction} />
      <Route exact path="/example-basic" component={ExampleBasic} />
    </Switch>
  );
}

export default Content;
