import React from 'react';
import { NavLink } from 'react-router-dom';

const navLinkProps = {
  exact: true,
  className: 'nav-link',
  activeClassName: 'active bg-primary text-white',
};

function Navigation() {
  return (
    <React.Fragment>
      <NavLink {...navLinkProps} to="/">Introduction</NavLink>
      <NavLink {...navLinkProps} to="/example-basic">Example: Basic Grid</NavLink>
    </React.Fragment>
  );
}

export default Navigation;
