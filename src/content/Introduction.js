import React from 'react';
import { Link } from 'react-router-dom';
import Prism from '@maji/react-prism'

function Introduction() {
  return (
    <React.Fragment>
      <h1>react-data-grid-multiline-header</h1>
      <h2>About</h2>
      <p>This is a helper class for <a href="https://github.com/adazzle/react-data-grid">react-data-grid</a> component that enables multiline text in grid header cells.</p>
      <h2>Why?</h2>
      <p>The original component doesn't allow header text that spans multiple lines. In some cases it forces you to make design compromises, expecially when heading is much wider than data.</p>
      <h2>How</h2>
      <p>This helper uses Canvas <a href="https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/measureText"><code>measureText()</code></a> method to get words' width in pixels and calculates dimensions of columns and grid header. Line breaking is achieved by simply overriding React Data Grid's CSS.</p>
      <h2>Installation</h2>
      <Prism>
        {"yarn add react-data-grid-multiline-header"}
      </Prism>
      <h2>Usage</h2>
      <p>Import styles once (for example in your app's <code>index.js</code>)</p>
      <Prism language="javascript">
        {"import 'react-data-grid-multiline-header/style.css';"}
      </Prism>
      <p>Import the class to your component</p>
      <Prism language="javascript">
        {"import Helper from 'react-data-grid-multiline-header';"}
      </Prism>
      <p>Initialize the helper</p>
      <Prism language="javascript">
        {"const helper = new Helper(columns, config);"}
      </Prism>
      <p>In render method, wrap <code>{"<ReactDataGrid>"}</code> element (this is necessary for styling)</p>
      <Prism language="jsx">
{`render() {
  return (
    <div className="react-grid-multiline-header">
      <ReactDataGrid ... />
    </div>
  );
}`}
      </Prism>
      <p>The helper exposes two properties to be consumed by <code>{"<ReactDataGrid>"}</code></p>
      <Prism language="jsx">
{`<ReactDataGrid
  headerRowHeight={helper.headerRowHeight}
  columns={helper.columns}
  ...
/>`}
      </Prism>
      <h2>Configuration</h2>
      <p>Options are passed as <code>config</code> object to constructor.</p>
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>Key</th>
            <th>Type</th>
            <th>Description</th>
            <th>Default value</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><code>minWidth</code></td>
            <td>Number</td>
            <td>Minimum column width in px.</td>
            <td><code>120</code></td>
          </tr>
          <tr>
            <td><code>maxWidth</code></td>
            <td>Number</td>
            <td>Maximum column width in px. <strong>Note:</strong> this is "soft" limit - column can get wider if there's a long single word in its header text.</td>
            <td><code>300</code></td>
          </tr>
          <tr>
            <td><code>hPaddingCell</code></td>
            <td>Number</td>
            <td>Header cell horizontal padding in px; used for calculating column width; defaults to react-data-grid style.</td>
            <td><code>16</code></td>
          </tr>
          <tr>
            <td><code>hPaddingSort</code></td>
            <td>Number</td>
            <td>Header cell extra horizontal padding for "sortable" columns.</td>
            <td><code>22</code></td>
          </tr>
          <tr>
            <td><code>vPaddingCell</code></td>
            <td>Number</td>
            <td>Header cell vertical padding in px; used for calculating <code>headerRowHeight</code>; defaults to react-data-grid style.</td>
            <td><code>16</code></td>
          </tr>
          <tr>
            <td><code>lineHeight</code></td>
            <td>Number</td>
            <td>Header cell line height in px; used for calculating <code>headerRowHeight</code>; defaults to Bootstrap style.</td>
            <td><code>24</code></td>
          </tr>
          <tr>
            <td><code>font</code></td>
            <td>String</td>
            <td>Header cell font style; used for calculating column width and number of lines; should be <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font">valid value for CSS <code>font</code> property</a>; defaults to Bootstrap style.</td>
            <td><code>700 16px -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif</code></td>
          </tr>
        </tbody>
      </table>
      <h2>Example</h2>
      <p>See <Link to="/example-basic">Example: Basic Grid</Link> for working example.</p>
    </React.Fragment>
  );
}

export default Introduction;
