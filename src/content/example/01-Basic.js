import Prism from '@maji/react-prism';
import keys from 'core-js/fn/array/keys'; // eslint-disable-line no-unused-vars
import from from 'core-js/fn/array/from'; // eslint-disable-line no-unused-vars
import React from 'react';
import ReactDataGrid from 'react-data-grid';
import Helper from 'react-data-grid-multiline-header';

class Grid extends React.Component {
  constructor(props) {
    super(props);
    this.createRows();

    this.columns = [
      { key: 'id', name: 'Identification number' },
      { key: 'title', name: 'Title' },
      { key: 'count', name: 'Count, calculated as identification number multiplied by 1000' },
    ];
    this.helper = new Helper(this.columns);
  }

  createRows = () => { this.rows = [...Array(50).keys()].map(v => ({ id: (v+1), title: 'Title ' + (v+1), count: (v+1) * 1000 })); }

  rowGetter = i => this.rows[i];

  render() {
    return (
      <div className="react-grid-multiline-header">
        <ReactDataGrid
          columns={this.helper.columns}
          headerRowHeight={this.helper.headerRowHeight}
          rowGetter={this.rowGetter}
          rowsCount={this.rows.length}
        />
      </div>
    );
  }
}

function ExampleBasic() {
  return (
    <React.Fragment>
      <h1>Example: Basic Grid</h1>
      <h2>Overview</h2>
      <p>In this example you can see how default <code>minWidth</code> and <code>maxWidth</code> take effect on columns' width: "Title" column is expanded to 120 pixels, "Count..." stops at 300 pixels and the text flows to next line, and "Identification number"'s width is somewhere in between, based on text measurement.</p>
      <Grid />
      <h2>Source code</h2>
      <Prism language="jsx">
{`import React from 'react';
import ReactDataGrid from 'react-data-grid';
import Helper from 'react-data-grid-multiline-header';

class Grid extends React.Component {
  constructor(props) {
    super(props);
    this.createRows();

    this.columns = [
      { key: 'id', name: 'Identification number' },
      { key: 'title', name: 'Title' },
      { key: 'count', name: 'Count, calculated as identification number multiplied by 1000' },
    ];
    this.helper = new Helper(this.columns);
  }

  createRows = () => { this.rows = [...Array(50).keys()].map(v => ({ id: (v+1), title: 'Title ' + (v+1), count: (v+1) * 1000 })); }

  rowGetter = i => this.rows[i];

  render() {
    return (
      <div className="react-grid-multiline-header">
        <ReactDataGrid
          columns={this.helper.columns}
          headerRowHeight={this.helper.headerRowHeight}
          rowGetter={this.rowGetter}
          rowsCount={this.rows.length}
        />
      </div>
    );
  }
}`}
      </Prism>
    </React.Fragment>
  );
}

export default ExampleBasic;
