import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Prism from 'prismjs'; // eslint-disable-line no-unused-vars
import 'prismjs/themes/prism.css';
import 'prismjs/components/prism-jsx';
import 'react-data-grid-multiline-header/style.css';

import App from './App';

ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, document.getElementById('root'));
