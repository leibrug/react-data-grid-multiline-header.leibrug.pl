import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <div className="navbar navbar-expand navbar-dark bg-dark">
      <div className="container">
        <Link to="/" className="navbar-brand">React Data Grid Multiline Header</Link>
        <div className="navbar-nav">
          <a className="nav-item nav-link" href="//gitlab.com/leibrug/react-data-grid-multiline-header">view on GitLab</a>
          <a className="nav-item nav-link" href="//leibrug.pl">by Leibrug</a>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
