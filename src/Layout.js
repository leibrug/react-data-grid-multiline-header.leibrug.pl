import React from 'react';

import './Layout.css';

function Layout(props) {
  return (
    <div className="Layout container">
      <div className="row">
        <div className="col-md-3">
          <nav className="nav flex-column">
            {props.children[0]}
          </nav>
        </div>
        <div className="col-md-9">
          {props.children[1]}
        </div>
      </div>
    </div>
  );
}

export default Layout;
