import React from 'react';

import Navbar from './Navbar';
import Layout from './Layout';
import Navigation from './Navigation';
import Content from './Content';

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <Layout>
          <Navigation />
          <Content />
        </Layout>
      </React.Fragment>
    );
  }
}

export default App;
